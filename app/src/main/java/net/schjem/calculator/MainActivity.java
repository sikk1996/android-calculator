package net.schjem.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.matheclipse.parser.client.eval.DoubleEvaluator;

public class MainActivity extends AppCompatActivity {
    TextView text_status;
    boolean help = false;
    boolean clearStatusFirst = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Calculator output
        text_status = findViewById(R.id.text_status);

        //Toolbar
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }


    public void btnClick(View view) {
        Log.d("Calculator","button "+view.toString()+" clicked");
        if (clearStatusFirst && !help && view.getId()!=R.id.btn_help){
            text_status.setText("");
            clearStatusFirst=false;
            Log.d("Calculator","Status cleared due to clearStatusFirst was set to true");

        }
        switch (view.getId()) {
            case R.id.btn_0:
                if (help) {
                    Toast.makeText(this, "Adds 0 to the calculation", Toast.LENGTH_LONG).show();
                    help = false;
                } else
                text_status.append("0");
                break;
            case R.id.btn_1:
                if (help) {
                    Toast.makeText(this, "Adds 1 to the calculation", Toast.LENGTH_LONG).show();
                    help = false;
                } else
                    text_status.append("1");
                break;
            case R.id.btn_2:
                if (help) {
                    Toast.makeText(this, "Adds 2 to the calculation", Toast.LENGTH_LONG).show();
                    help = false;
                } else
                    text_status.append("2");
                break;
            case R.id.btn_3:
                if (help) {
                    Toast.makeText(this, "Adds 3 to the calculation", Toast.LENGTH_LONG).show();
                    help = false;
                } else
                    text_status.append("3");
                break;
            case R.id.btn_4:
                if (help) {
                    Toast.makeText(this, "Adds 4 to the calculation", Toast.LENGTH_LONG).show();
                    help = false;
                } else
                    text_status.append("4");
                break;
            case R.id.btn_5:
                if (help) {
                    Toast.makeText(this, "Adds 5 to the calculation", Toast.LENGTH_LONG).show();
                    help = false;
                } else
                    text_status.append("5");
                break;
            case R.id.btn_6:
                if (help) {
                    Toast.makeText(this, "Adds 6 to the calculation", Toast.LENGTH_LONG).show();
                    help = false;
                } else
                    text_status.append("6");
                break;
            case R.id.btn_7:
                if (help) {
                    Toast.makeText(this, "Adds 7 to the calculation", Toast.LENGTH_LONG).show();
                    help = false;
                } else
                    text_status.append("7");
                break;
            case R.id.btn_8:
                if (help) {
                    Toast.makeText(this, "Adds 8 to the calculation", Toast.LENGTH_LONG).show();
                    help = false;
                } else
                    text_status.append("8");
                break;
            case R.id.btn_9:
                if (help) {
                    Toast.makeText(this, "Adds 9 to the calculation", Toast.LENGTH_LONG).show();
                    help = false;
                } else
                    text_status.append("9");
                break;
            case R.id.btn_delete:
                if (help) {
                    Toast.makeText(this, "Clears the calculation", Toast.LENGTH_LONG).show();
                    help = false;
                } else
                    text_status.setText("");
                break;
            case R.id.btn_divide:
                if (help) {
                    Toast.makeText(this, "Division", Toast.LENGTH_LONG).show();
                    help = false;
                } else
                    text_status.append("/");
                break;
            case R.id.btn_equals:
                if (help) {
                    Toast.makeText(this, "Calculates and shows result", Toast.LENGTH_LONG).show();
                    help = false;
                } else{
                    //SOLVE
                    DoubleEvaluator mathEngine = new DoubleEvaluator();
                    String calcThis = text_status.getText().toString();
                    Double result = mathEngine.evaluate(calcThis);
                    Log.d("Calculator_calculation",result+"");
                    text_status.setText(result+"");
                }
                break;
            case R.id.btn_exit:
                if (help) {
                    Toast.makeText(this, "Exits the app", Toast.LENGTH_LONG).show();
                    help = false;
                } else
                    this.finish();
                break;
            case R.id.btn_help:
                if (help) {
                    Toast.makeText(this, "This is the help button u idiot", Toast.LENGTH_LONG).show();
                    help = false;
                } else
                    Toast.makeText(this, "Press button you want help with", Toast.LENGTH_LONG).show();
                help = true;
                break;
            case R.id.btn_plus:
                if (help) {
                    Toast.makeText(this, "Addition", Toast.LENGTH_LONG).show();
                    help = false;
                } else
                    text_status.append("+");
                break;
            case R.id.btn_minus:
                if (help) {
                    Toast.makeText(this, "Subtraction", Toast.LENGTH_LONG).show();
                    help = false;
                } else
                    text_status.append("-");
                break;
            case R.id.btn_multiply:
                if (help) {
                    Toast.makeText(this, "Multiplication", Toast.LENGTH_LONG).show();
                    help = false;
                } else
                    text_status.append("*");
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_action_settings:
                Log.d("Calculator_actionBar","Settings button clicked");
                Toast.makeText(this,"settings menu",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.menu_action_help:
                Log.d("Calculator_actionBar","help button clicked");
                Toast.makeText(this,"help menu",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.menu_layout_normal:
                Log.d("Calculator_actionBar","normal layout button clicked");
                Toast.makeText(this,"Normal layout",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.menu_layout_science:
                Log.d("Calculator_actionBar","science layout button clicked");
                Toast.makeText(this,"Science layout",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.menu_layout_unitConverting:
                Log.d("Calculator_actionBar","unit convering layout button clicked");
                Toast.makeText(this,"Unit-Converting layout",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.menu_exit:
                Log.d("Calculator_actionBar","exit button clicked");
                this.finish();
                return true;
            default:
                Log.d("Calculator_actionBar","invoking action-menus superclass");
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
}
